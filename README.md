# Assignment to become a Memsource Developer
## How to run
To run Memsource project you need to have installed Java 13 or later.

Use `java -jar memsource.jar` 

Project root URL is `http://localhost:8080`

## How to compile and run
There is also possibility compile and run source code using Maven.

Use `mvn spring-boot:run`

Project root URL is `http://localhost:8080`

## REST API documentation
Memsource projects offers two REST API endpoint:

* `PUT /api/user/{username}/password` to change user password 
* `GET /api/project` to download available project from Memsource API

Draft of swagger documentation can be found at: `http://localhost:8080/swagger-ui/index.html` after project starts.  

## About
### H2 Database
Memsource project uses H2 in-memory database to store data. Initial user `tester` with password `foobar` is created 
during project initialization and is recreated when project restarts.

H2 console is accessible at `http://localhost:8080//h2-console` with user `sa` and password `memsource`.

### application.properties
File `application.properties` contains configuration properties of project. There are some properties related to
project itself but there are two properties that configures connection to Memsource API.

* `memsource.api-url` defines URL of Memsource API
* `memsource.credentials` defines user and password to Memsource API in format `>>user<<,>>password<<`

### web
There are two HTML pages located in `web` directory

* `settings.html` offers form that allows change user password
* `projects.html` that downloads and show projects from Memsource via Project REST API
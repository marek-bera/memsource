package cz.bera.memsource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan("cz.bera.memsource")
public class App {

  public static void main(String[] args) {
    SpringApplication.run(App.class);
  }
}

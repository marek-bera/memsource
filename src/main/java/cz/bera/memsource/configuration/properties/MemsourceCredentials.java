package cz.bera.memsource.configuration.properties;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class MemsourceCredentials {

  String userName;
  String password;
}

package cz.bera.memsource.configuration.properties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class MemsourceProperties {

  private MemsourceCredentials credentials;
  private String apiUrl;

}

package cz.bera.memsource.configuration;

import cz.bera.memsource.configuration.properties.MemsourceCredentials;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@ConfigurationPropertiesBinding
public class MemsourceCredentialsConverter implements Converter<String, MemsourceCredentials> {

  @Override
  public MemsourceCredentials convert(String source) {
    final String[] data = source.split(",");
    return MemsourceCredentials
        .builder()
        .userName(data[0])
        .password(data[1])
        .build();
  }

}

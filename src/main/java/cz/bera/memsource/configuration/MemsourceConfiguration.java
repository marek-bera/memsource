package cz.bera.memsource.configuration;

import cz.bera.memsource.configuration.properties.MemsourceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class MemsourceConfiguration {

  @Autowired
  private MessageSource messageSource;

  @Bean
  @ConfigurationProperties(prefix = "memsource")
  public MemsourceProperties applicationProperties() {
    return new MemsourceProperties();
  }

  @Bean
  public LocalValidatorFactoryBean getValidator() {
    LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
    validator.setValidationMessageSource(messageSource);

    return validator;
  }
}

package cz.bera.memsource.configuration;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

@Configuration
public class OkHttpConfiguration {

  @Bean
  public OkHttpClient okHttpClient() {
    return new OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor())
        .addInterceptor(headerAcceptJson())
        .build();
  }

  @Bean
  public Interceptor headerAcceptJson() {
    return chain -> chain.proceed(
        chain
            .request()
            .newBuilder()
            .addHeader("Accept", MediaType.APPLICATION_JSON_VALUE)
            .build()
    );
  }

  @Bean
  public Interceptor loggingInterceptor() {
    return new HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BASIC);
  }
}

package cz.bera.memsource.service;

import cz.bera.memsource.controller.model.ProjectResponse;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ProjectService {

  @NotNull
  List<ProjectResponse> getAllMemsourceProjects();
}

package cz.bera.memsource.service;

import cz.bera.memsource.retrofit.model.LoginRequestMO;
import cz.bera.memsource.retrofit.model.LoginResponseMO;
import cz.bera.memsource.retrofit.model.LoginResponseUserMO;
import cz.bera.memsource.retrofit.model.ProjectResponseMO;

import javax.validation.constraints.NotNull;

public interface MemsourceService {

  /**
   * Authenticates to Memsource API and retrieves authenticated {@link LoginResponseUserMO}
   * and {@code String} authentication token.
   *
   * @param loginRequest Authentication request object that provides {@link String} userName, {@link String} password
   * and {@link String} code.
   * @return Object that contains authenticated {@link LoginResponseUserMO}
   * and {@link String} token.
   */
  @NotNull
  LoginResponseMO login(LoginRequestMO loginRequest);

  /**
   * Retrieves list of memsource projects. See <a href="https://cloud.memsource.com/web/docs/api#operation/listProjects">https://cloud.memsource.com/web/docs/api#operation/listProjects/a>
   * @return Object that contains memsource project response.
   */
  @NotNull
  ProjectResponseMO getProjects(@NotNull String apiToken);
}

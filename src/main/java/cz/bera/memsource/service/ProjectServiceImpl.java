package cz.bera.memsource.service;

import cz.bera.memsource.configuration.properties.MemsourceProperties;
import cz.bera.memsource.controller.model.ProjectResponse;
import cz.bera.memsource.retrofit.model.LoginRequestMO;
import cz.bera.memsource.retrofit.model.LoginResponseMO;
import cz.bera.memsource.retrofit.model.ProjectContentResponseMO;
import cz.bera.memsource.retrofit.model.ProjectResponseMO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProjectServiceImpl extends AbstractService implements ProjectService {

  @Autowired
  private MemsourceProperties memsourceProperties;

  @Autowired
  private MemsourceService memsourceService;

  @Override
  public List<ProjectResponse> getAllMemsourceProjects() {
    final LoginResponseMO loginResponse = memsourceService.login(
        LoginRequestMO
            .builder()
            .userName(memsourceProperties.getCredentials().getUserName())
            .password(memsourceProperties.getCredentials().getPassword())
            .build()
    );

    final ProjectResponseMO projects = memsourceService.getProjects(loginResponse.getToken());

    return projects
        .getContent()
        .stream()
        .map(this::map)
        .collect(Collectors.toUnmodifiableList());
  }

  @NotNull
  private ProjectResponse map(@NotNull final ProjectContentResponseMO projectContent) {
    return ProjectResponse
        .builder()
        .name(projectContent.getName())
        .status(projectContent.getStatus().toString())
        .sourceLang(projectContent.getSourceLang())
        .targetLangs(projectContent.getTargetLangs())
        .build();
  }
}

package cz.bera.memsource.service;

import cz.bera.memsource.controller.model.PasswordChangeRequest;
import cz.bera.memsource.exception.BadRequestException;
import cz.bera.memsource.exception.NotFoundException;
import cz.bera.memsource.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Service
@Slf4j
public class UserServiceImpl extends AbstractService implements UserService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public void updatePassword(
      @NotNull final String username,
      @NotNull final PasswordChangeRequest passwordChangeRequest
  ) {
    userRepository
        .findByUsername(username)
        .map(userVO -> {
          if (!userVO.getPassword().equals(passwordChangeRequest.getOldPassword())) {
            throw new BadRequestException(
                messageSource.getMessage(
                    "error.request.password.change",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
          }
          userVO.setPassword(passwordChangeRequest.getNewPassword());

          return userRepository.saveAndFlush(userVO);
        })
        .orElseThrow(() -> new NotFoundException(
            messageSource.getMessage(
                "error.jpa.user.not-found",
                new Object[]{username},
                LocaleContextHolder.getLocale()

            )
        ));

    log.info("Password successfully changed for user: {}", username);
  }
}

package cz.bera.memsource.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

public abstract class AbstractService {

  @Autowired
  protected MessageSource messageSource;

}

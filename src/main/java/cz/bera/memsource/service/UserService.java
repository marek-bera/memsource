package cz.bera.memsource.service;

import cz.bera.memsource.controller.model.PasswordChangeRequest;

import javax.validation.constraints.NotNull;

public interface UserService {

  /**
   * Updates user's password.
   *
   * @param username {@link String} username
   * @param passwordChangeRequest {@link PasswordChangeRequest} that contains old password and now password.
   */
  @NotNull
  void updatePassword(
      @NotNull String username,
      @NotNull PasswordChangeRequest passwordChangeRequest
  );
}

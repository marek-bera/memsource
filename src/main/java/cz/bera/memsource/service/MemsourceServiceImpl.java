package cz.bera.memsource.service;

import cz.bera.memsource.configuration.properties.MemsourceProperties;
import cz.bera.memsource.exception.BadGatewayException;
import cz.bera.memsource.exception.UnauthorizedException;
import cz.bera.memsource.retrofit.RetrofitClientCreator;
import cz.bera.memsource.retrofit.api.MemsourceApi;
import cz.bera.memsource.retrofit.model.LoginRequestMO;
import cz.bera.memsource.retrofit.model.LoginResponseMO;
import cz.bera.memsource.retrofit.model.ProjectResponseMO;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import javax.validation.constraints.NotNull;
import java.io.IOException;

@Slf4j
@Service
public class MemsourceServiceImpl extends AbstractService implements MemsourceService {

  private static final String API_TOKEN_PREFIX = "ApiToken ";

  @Autowired
  private MemsourceProperties memsourceProperties;

  @Autowired
  private OkHttpClient okHttpClient;

  @NotNull
  @Override
  public LoginResponseMO login(LoginRequestMO loginRequest) {
    MemsourceApi memsourceApi = RetrofitClientCreator.createClient(MemsourceApi.class, memsourceProperties.getApiUrl(), okHttpClient);

    try {
      Response<LoginResponseMO> response = memsourceApi.login(loginRequest).execute();
      log.debug("Memsource Login response: {}", response);
      if (!response.isSuccessful()) {
        log.warn("Response code: {}\nResponse: {}", response.code(), response.errorBody().string());
        if (response.code() == 401) {
          throw new UnauthorizedException(response.errorBody().string());
        } else {
          throw new BadGatewayException(response.errorBody().string());
        }
      }

      return response.body();
    } catch (IOException ex) {
      log.error("Memsource login request error: {}", ex.getMessage());
      throw new BadGatewayException(ex.getMessage());
    }
  }

  @NotNull
  @Override
  public ProjectResponseMO getProjects(@NotNull final String apiToken) {
    MemsourceApi memsourceApi = RetrofitClientCreator.createClient(MemsourceApi.class, memsourceProperties.getApiUrl(), okHttpClient);

    try {
      Response<ProjectResponseMO> response = memsourceApi.getProjects(API_TOKEN_PREFIX + apiToken).execute();
      log.debug("Memsource GET projects response: {}", response);
      if (!response.isSuccessful()) {
        log.warn("Response code: {}\nResponse: {}", response.code(), response.errorBody().string());
        if (response.code() == 401) {
          throw new UnauthorizedException(response.errorBody().string());
        } else {
          throw new BadGatewayException(response.errorBody().string());
        }
      }

      return response.body();
    } catch (IOException ex) {
      log.error("Memsource GET projects error: {}", ex.getMessage());
      throw new BadGatewayException(ex.getMessage());
    }
  }
}

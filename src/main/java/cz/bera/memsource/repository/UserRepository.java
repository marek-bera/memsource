package cz.bera.memsource.repository;

import cz.bera.memsource.repository.model.UserVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserVO, Long> {

  Optional<UserVO> findByUsername(@NotNull String username);
}

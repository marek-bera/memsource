package cz.bera.memsource.retrofit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponseMO {

  private LoginResponseUserMO user;
  private String token;
}

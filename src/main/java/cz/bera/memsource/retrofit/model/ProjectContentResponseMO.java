package cz.bera.memsource.retrofit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.bera.memsource.retrofit.model.enums.JobStatusMO;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectContentResponseMO {

  private String uid;
  private Long internetId;
  private String id;
  private String name;
  private ProjectContentDomainResponseMO domain;
  private ProjectContentDomainResponseMO subDomain;
  private LoginResponseUserMO owner;
  private String sourceLang;
  private List<String> targetLangs;
  private List<ProjectContentReferenceMO> references;
  private List<ProjectContentSettingsMO> mtSettingsPerLanguageList;
  private String userRole;
  private JobStatusMO status;
}

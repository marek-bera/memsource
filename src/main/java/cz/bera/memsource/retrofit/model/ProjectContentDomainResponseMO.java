package cz.bera.memsource.retrofit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectContentDomainResponseMO {

  private String name;
  private String id;
  private String uid;
}

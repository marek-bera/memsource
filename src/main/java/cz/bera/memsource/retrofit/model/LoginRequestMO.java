package cz.bera.memsource.retrofit.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class LoginRequestMO {

  String userName;
  String password;
  String code;
}

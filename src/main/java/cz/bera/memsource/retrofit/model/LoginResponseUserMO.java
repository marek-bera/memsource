package cz.bera.memsource.retrofit.model;

import lombok.Data;

@Data
public class LoginResponseUserMO {

  private String firstName;
  private String lastName;
  private String userName;
  private String email;
  private String role;
  private String id;
  private String uid;

}

package cz.bera.memsource.retrofit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectResponseMO {

  private Integer totalElements;
  private Integer totalPages;
  private Integer pageSize;
  private Integer pageNumber;
  private Integer numberOfElements;
  private List<ProjectContentResponseMO> content;
}

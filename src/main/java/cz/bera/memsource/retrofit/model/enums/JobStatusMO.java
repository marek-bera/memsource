package cz.bera.memsource.retrofit.model.enums;

public enum JobStatusMO {

  NEW,
  ASSIGNED,
  COMPLETED,
  CANCELED
}

package cz.bera.memsource.retrofit.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectContentReferenceMO {

  private String id;
  private String filename;
  private String note;
}

package cz.bera.memsource.retrofit.api;

import cz.bera.memsource.retrofit.model.LoginRequestMO;
import cz.bera.memsource.retrofit.model.LoginResponseMO;
import cz.bera.memsource.retrofit.model.ProjectResponseMO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MemsourceApi {

  /**
   * Login to Memsource API.
   * See <a href="https://cloud.memsource.com/web/docs/api#operation/login">https://cloud.memsource.com/web/docs/api#operation/login/a>
   *
   * @param loginRequest Login credentials
   *
   * @return Memsource API login response
   */
  @POST("api2/v1/auth/login")
  Call<LoginResponseMO> login(@Body LoginRequestMO loginRequest);

  /**
   * Get list of Memsource project for user based on {@link String} apiToken
   *
   * @param apiToken Memsource API token received via {@link #login(LoginRequestMO)}
   *
   * @return Memsource API response
   */
  @GET("api2/v1/projects")
  Call<ProjectResponseMO> getProjects(
      @Header("Authorization") String apiToken
  );
}

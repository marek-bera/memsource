package cz.bera.memsource.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitClientCreator {

  public static <T> T createClient(final Class<T> clazz, final String baseUrl, OkHttpClient client) throws IllegalArgumentException {
    return new Retrofit
        .Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(JacksonConverterFactory.create())
        .client(client)
        .build()
        .create(clazz);
  }

}

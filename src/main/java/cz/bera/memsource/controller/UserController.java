package cz.bera.memsource.controller;

import cz.bera.memsource.controller.model.PasswordChangeRequest;
import cz.bera.memsource.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class UserController {

  @Autowired
  private UserService userService;

  @CrossOrigin
  @PutMapping(value = "/{username}/password", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> updateUserPassword(
      @PathVariable String username,
      @Valid @RequestBody PasswordChangeRequest passwordChangeRequest
  ) {
    log.debug("PUT /api/user/{}", username);

    userService.updatePassword(username, passwordChangeRequest);

    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }
}

package cz.bera.memsource.controller;

import cz.bera.memsource.controller.model.ApiErrorDetails;
import cz.bera.memsource.exception.BadGatewayException;
import cz.bera.memsource.exception.BadRequestException;
import cz.bera.memsource.exception.NotFoundException;
import cz.bera.memsource.exception.UnauthorizedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request
  ) {
    final List<String> errors = new ArrayList<>();
    ex.getBindingResult().getFieldErrors()
        .stream()
        .map(fieldError -> fieldError.getField() + ": " + fieldError.getDefaultMessage())
        .collect(Collectors.toCollection(() -> errors));

    ex.getBindingResult().getGlobalErrors()
        .stream()
        .map(objectError ->objectError.getObjectName() + ": " + objectError.getDefaultMessage())
        .collect(Collectors.toCollection(() -> errors));

    final ApiErrorDetails error = new ApiErrorDetails(
        LocalDateTime.now(),
        HttpStatus.BAD_REQUEST,
        ex.getLocalizedMessage(),
        errors
    );

    return handleExceptionInternal(ex, error, headers, error.getHttpStatus(), request);
  }

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request
  ) {
    final ApiErrorDetails error = new ApiErrorDetails(
        LocalDateTime.now(),
        HttpStatus.BAD_REQUEST,
        ex.getLocalizedMessage(),
        ex.getParameterName() + " parameter is missing!"
    );

    return createResponse(error);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request
  ) {
    final ApiErrorDetails error = new ApiErrorDetails(
        LocalDateTime.now(),
        HttpStatus.BAD_REQUEST,
        ex.getMostSpecificCause().getClass().getName(),
        ex.getMostSpecificCause().getMessage()
    );
    return createResponse(error);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<Object> handleConstraintViolation(
      ConstraintViolationException ex,
      WebRequest request
  ) {

    final List<String> errors = new ArrayList<>();
    ex.getConstraintViolations()
        .stream()
        .map(violation -> violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": " + violation.getMessage())
        .collect(Collectors.toCollection(() -> errors));

    final ApiErrorDetails error = new ApiErrorDetails(
        LocalDateTime.now(),
        HttpStatus.BAD_REQUEST,
        ex.getLocalizedMessage(),
        errors
    );

    return createResponse(error);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
      MethodArgumentTypeMismatchException ex,
      WebRequest request
  ) {

    final ApiErrorDetails error = new ApiErrorDetails(
        LocalDateTime.now(),
        HttpStatus.BAD_REQUEST,
        ex.getLocalizedMessage(),
        ex.getName() + " should by of type " + ex.getRequiredType().getName()
    );

    return createResponse(error);
  }

  @ExceptionHandler(BadGatewayException.class)
  public ResponseEntity<Object> handleBadGatewayException(final BadGatewayException ex) {
    ApiErrorDetails error = new ApiErrorDetails(LocalDateTime.now(), HttpStatus.BAD_GATEWAY, ex.getLocalizedMessage());

    return createResponse(error);
  }

  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<Object> handleBadRequestException(final BadRequestException ex) {
    ApiErrorDetails error = new ApiErrorDetails(LocalDateTime.now(), HttpStatus.BAD_REQUEST, ex.getLocalizedMessage());

    return createResponse(error);

  }

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<Object> handleNotFoundException(final NotFoundException ex) {
    ApiErrorDetails error = new ApiErrorDetails(LocalDateTime.now(), HttpStatus.NOT_FOUND, ex.getLocalizedMessage());

    return createResponse(error);
  }

  @ExceptionHandler(UnauthorizedException.class)
  public ResponseEntity<Object> handleUnauthorizedException(final UnauthorizedException ex) {
    ApiErrorDetails error = new ApiErrorDetails(LocalDateTime.now(), HttpStatus.UNAUTHORIZED, ex.getLocalizedMessage());

    return createResponse(error);
  }

  private ResponseEntity<Object> createResponse(@NotNull ApiErrorDetails error) {
    return new ResponseEntity<>(error, new HttpHeaders(), error.getHttpStatus());
  }
}

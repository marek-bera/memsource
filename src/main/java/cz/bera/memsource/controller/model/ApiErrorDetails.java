package cz.bera.memsource.controller.model;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Value
@AllArgsConstructor
public class ApiErrorDetails {

  LocalDateTime timestamp;
  HttpStatus httpStatus;
  String message;
  List<String> errors;

  public ApiErrorDetails(LocalDateTime timestamp, HttpStatus httpStatus, String message, String error) {
    super();
    this.timestamp = timestamp;
    this.httpStatus = httpStatus;
    this.message = message;
    errors = List.of(error);
  }

  public ApiErrorDetails(LocalDateTime timestamp, HttpStatus httpStatus, String message) {
    super();
    this.timestamp = timestamp;
    this.httpStatus = httpStatus;
    this.message = message;
    errors = new ArrayList<>();
  }
}

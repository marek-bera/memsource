package cz.bera.memsource.controller.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class PasswordChangeRequest {

  @NotBlank(message = "{error.request.password.old}")
  private String oldPassword;

  @NotBlank(message = "{error.request.password.new}")
  @Size(min = 4, message = "{error.request.password.short}")
  private String newPassword;
}

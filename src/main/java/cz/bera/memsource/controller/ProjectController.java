package cz.bera.memsource.controller;

import cz.bera.memsource.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/project", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class ProjectController {

  @Autowired
  private ProjectService projectService;

  @CrossOrigin
  @GetMapping
  public ResponseEntity<?> getMemsourceProjects() {
    log.debug("GET /api/project");

    return ResponseEntity.ok(projectService.getAllMemsourceProjects());
  }
}

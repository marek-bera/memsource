package cz.bera.memsource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.bera.memsource.controller.model.PasswordChangeRequest;
import cz.bera.memsource.repository.UserRepository;
import cz.bera.memsource.repository.model.UserVO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ControllersTest {

  private static final String USERNAME = "testname";

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private UserRepository userRepository;

  @Order(1)
  @DisplayName("Test script loaded")
  @Test
  @Sql("/data-test.sql")
  public void testScriptLoaded_thanSuccess() {
    Optional<UserVO> username = userRepository.findByUsername(USERNAME);
    assertTrue(username.isPresent());
    assertEquals(USERNAME, username.get().getUsername());
  }

  @Order(2)
  @DisplayName("Test API change user password successfully")
  @Test
  public void userController_updateUserPassword_then204() throws Exception {
    final PasswordChangeRequest request = new PasswordChangeRequest();
    request.setOldPassword("foobar");
    request.setNewPassword("newbar");

    mockMvc
        .perform(
            MockMvcRequestBuilders
                .put("/api/user/" + USERNAME + "/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(request))
        )
        .andExpect(
            MockMvcResultMatchers
                .status()
                .isNoContent()
        );
  }

  @DisplayName("Test API change user password - wrong old password")
  @Test
  public void userControoler_updatePassword_then400() throws Exception {
    final PasswordChangeRequest request = new PasswordChangeRequest();
    request.setOldPassword("wrongpassword");
    request.setNewPassword("newpasswword");

    mockMvc
        .perform(
            MockMvcRequestBuilders
            .put("/api/user/" + USERNAME + "/password")
            .contentType(MediaType.APPLICATION_JSON)
            .content(toJson(request))
        )
        .andExpect(
            MockMvcResultMatchers
              .status()
              .isBadRequest()
        );
  }

  @DisplayName("Test API change user password - wrong username")
  @Test
  public void userControoler_updatePassword_then404() throws Exception {
    final PasswordChangeRequest request = new PasswordChangeRequest();
    request.setOldPassword("notused");
    request.setNewPassword("notused");

    mockMvc
        .perform(
            MockMvcRequestBuilders
                .put("/api/user/invalid/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(request))
        )
        .andExpect(
            MockMvcResultMatchers
                .status()
                .isNotFound()
        );
  }

  @DisplayName("Test API get memsource projects")
  @Test
  public void projectController_getMemsourceProjects_then200() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders
                .get("/api/project")
                .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(
            MockMvcResultMatchers
                .status()
                .isOk()
        );
  }


  private static byte[] toJson(Object object) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    return mapper.writeValueAsBytes(object);
  }
}

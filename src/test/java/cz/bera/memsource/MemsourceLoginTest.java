package cz.bera.memsource;

import cz.bera.memsource.configuration.properties.MemsourceProperties;
import cz.bera.memsource.exception.UnauthorizedException;
import cz.bera.memsource.retrofit.model.LoginRequestMO;
import cz.bera.memsource.retrofit.model.LoginResponseMO;
import cz.bera.memsource.retrofit.model.ProjectResponseMO;
import cz.bera.memsource.service.MemsourceService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class MemsourceLoginTest {

  @Autowired
  private MemsourceProperties memsourceProperties;

  @Autowired
  private MemsourceService memsourceService;

  @DisplayName("Test login to memsource api")
  @Test
  public void memsourceLogin_thenReturnLoginResponse() {
    LoginRequestMO loginRequest = LoginRequestMO
        .builder()
        .userName(memsourceProperties.getCredentials().getUserName())
        .password(memsourceProperties.getCredentials().getPassword())
        .build();

    final LoginResponseMO loginResponse = memsourceService.login(loginRequest);
    assertNotNull(loginResponse);
    assertNotNull(loginResponse.getToken());
    assertEquals(memsourceProperties.getCredentials().getUserName(), loginResponse.getUser().getUserName());
  }

  @DisplayName("Test GET memsource projects")
  @Test
  public void memsourceGetProjects_thenUnauthorizedException() {
    assertThrows(UnauthorizedException.class, () -> memsourceService.getProjects("invalid"));
  }

  @DisplayName("Test login and GET project")
  @Test
  public void memsourceLoginAndGetProjects_success() {
    LoginRequestMO loginRequest = LoginRequestMO
        .builder()
        .userName(memsourceProperties.getCredentials().getUserName())
        .password(memsourceProperties.getCredentials().getPassword())
        .build();

    final LoginResponseMO loginResponse = memsourceService.login(loginRequest);
    assertNotNull(loginResponse);
    assertNotNull(loginResponse.getToken());

    final ProjectResponseMO projects = memsourceService.getProjects(loginResponse.getToken());
    assertNotNull(projects);
  }
}
